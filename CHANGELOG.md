# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.9.0"></a>

# [1.9.0](https://github.com/Kocal/Solary/compare/v1.8.1...v1.9.0) (2018-05-03)

### Bug Fixes

* **popup:** remove unused and breaking « type="ts" » ([a634316](https://github.com/Kocal/Solary/commit/a634316))
* **popup:navigation:** fill icons in white ([36cf98e](https://github.com/Kocal/Solary/commit/36cf98e))

### Refactor

* **projects:** remove everything about server project ([ce45016](https://github.com/Kocal/solary/commit/ce45016))
* **extension:** remove websocket things ([7c92cf0](https://github.com/Kocal/solary/commit/7c92cf0))
* remove lerna and reorganize project ([c45268e](https://github.com/Kocal/solary/commit/c45268e))
